#@field POWERMETER_NAME
#@type STRING
#@field POWERMETER_ADDRESS
#@type STRING
#@field POWERMETER_POLL1
#@type STRING
#@field POWERMETER_POLL2
#@type STRING

#Prefix for EPICS PVs.
#

 
#Configure the Modbus driver
#drvModbusAsynConfigure( portName,                        tcpPortName, slaveAddress,                     modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec,               plcType);
drvModbusAsynConfigure( "$(POWERMETER_NAME)-IME_DATA1",  "tcpcon1",    $(POWERMETER_ADDRESS),            3,              4096,              34,           0,        $(POWERMETER_POLL1),     "")
drvModbusAsynConfigure( "$(POWERMETER_NAME)-IME_DATA2",  "tcpcon1",    $(POWERMETER_ADDRESS),            3,              4130,              28,           0,        $(POWERMETER_POLL2),     "")

#Load the database defining your EPICS records
dbLoadRecords(ime-powermeter.template, "P=$(POWERMETER_NAME)")

